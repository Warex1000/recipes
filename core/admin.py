from django.contrib import admin

from . import models


@admin.register(models.Recipe)
class RecipeAdmin(admin.ModelAdmin):
    class IngredientInline(admin.TabularInline):
        model = models.Ingredient

    inlines = [IngredientInline]

