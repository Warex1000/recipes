urlcaller.py
import random
import sys
import requests


 passwords = passwords_data.split('\n')
 print(passwords[0:10])


class BadPasswordGenerator:
     def __init__(self):
       self.i = 0
        with open(text.txt) as D:
            passwords_data = D.read()
         self.passwords = passwords_data.split('\n')

     def next(self):
        password = self.passwords[self.i]
        self.i += 1
         return password


# generator = BadPasswordGenerator()
# print(generator.next())
# print(generator.i)

class GoodPasswordGenerator:
    def __init__(self):
        self.alfabet = '1234567890' \
                       'qwertyuiop' \
                       'asdfghjklz' \
                       'xcvbnm,QWE' \
                       'RTYUIOPASDF' \
                       'GHJKL!@#$%^' \
                       '&*()_+'

    def next(self, length=10):
        password = ''
        for i in range(length):
            c = random.choice(self.alfabet)
            password += c
        return password


# generator2 = GoodPasswordGenerator()
# print(generator2.next())
# print(generator2.next())