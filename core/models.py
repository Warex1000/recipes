from django.db import models


class Recipe(models.Model):
    name = models.CharField('название', max_length=250)
    description = models.TextField('описание', blank=True, null=True)

    class Meta:
        verbose_name = 'рецепт'
        verbose_name_plural = 'рецепты'

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField('название', max_length=250)
    amount = models.CharField('колличество', max_length=250)
    recipe = models.ForeignKey('Recipe', on_delete=models.CASCADE, verbose_name='рецепт')

    class Meta:
        verbose_name = 'ингридиент'
        verbose_name_plural = 'ингридиенты'

    def __str__(self):
        return self.name