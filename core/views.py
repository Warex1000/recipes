from django.views.generic import TemplateView
from .models import Recipe

class Index(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['recipes'] = Recipe.objects.all()
        return ctx
