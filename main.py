import shpora2
import city_temp

x = shpora2.object_car

if __name__ == '__main__':
    print(f'{round(shpora2.celsius_to_fahrenheit(celsius=45), 2)} Fahrenheit')
    print(f'{round(shpora2.fahrenheit_to_celsius(fahrenheit=200), 2)} C')
    print(f'{shpora2.letters_in_word("mather")} number')
