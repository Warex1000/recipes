import shpora2  # импорт всего файла v1
# from .shpora2 import fahrenheit  # импорт переменной из файла
# from .shpora2 import * # импорт всех переменных из файла
# pip install -r requirements.txt  # установка требований проекта (зависимостей)

"""
Документация
"""
None  # Пустое значение
True, False  # boolean, bool, булево значение (Истина и Ложь)
1  # integer, int, целое цисло
'1' "1"  # string, str, строка
1.0  # floating point, float, число с плавающей запятой (Дробное)
[1, 2, 3]  # list, список (изменяемый тип данных mutable)
(1, 2, 3)  # tuple, кортеж (не изменяемый тип данных immutable)
{1, 2, 3}  # set, сет (нельзя записать 2 одинаковых елемента)
{'key 1': 'value1', 'key2': 2}  # dictionary, dict, словарь (сопоставляет ключи и значения)


x = 1  # обявление переменной Х и присвоение ей значение 1
y = x  # обявление переменной Y и присвоение ей текущего значения переменной Х


def function_name(argument1, argument2=1):
    """
    обявление функции с названием function_name и обязательным аргументом argument1
    и не обязательным аргументом argument2
    """
    return argument1


class ClassName(ParentClass):  # объявление класса ClassName который наследуется от класса ParentClass
    attribute1 = 1  # обявление атрибута attribute1

    def __init__(self, attribute2):
        self.attribute2 = attribute2

    def method_name(self, argument1, argument2=1):
        """
        обявление метода с названием method_name и обязательным аргументом argument1
        и не обязательным аргументом argument2
        """
        return argument1


if a == b: # условие что а ровняется b ?
    # это выполнится если а == b.
elif c == d:
    # выполнится если a != b и с == d (! - это 'не')
else:
    # выполнится во всех остальных случаях


while a == b:
    # будет покругу выполнятся пока а не перестанет == b


for element in [1, 2, 3]:
    # будет выполнятся по кругу для каждого елемента из списка, в element будет записан текущий елемент списка


# class A:
#     name = ''
#     def say_name(self):
#         return self.name
# x = A()
# y = A()
# x.name = 'igor'
# x.say_name()
# 'igor'
# y.name = 'stepan'
# y.say_name()
# 'stepan'


