def celsius_to_fahrenheit(celsius=25):
    """ 12°Cx1.8 + 32 = x°F """
    fahrenheit = celsius * 1.8 + 32
    return fahrenheit


def fahrenheit_to_celsius(fahrenheit=12):
    """ (°F − 32) × 5/9 = °C """
    return (fahrenheit - 32) * 5/9  # example #2


def letters_in_word(word):
    return len(word)


class Auto:
    fuel = 'gasoline'  # аттрибут каждой машины


class PassengerCar(Auto):
    wheels_amount = 4

    def __init__(self, number):
        self.number = number


object_car = PassengerCar('АН3322ОО')